var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { InformationTechnologyComponent } from './departments/information-technology/information-technology.component';
import { SalesComponent } from './departments/sales/sales.component';
import { AdministrationComponent } from './departments/administration/administration.component';
import { FinanceComponent } from './departments/finance/finance.component';
import { WorkersComponent } from './workers.component';
var appRoutesWorker = [
    {
        path: 'workers',
        component: WorkersComponent,
        children: [{
                path: 'administration',
                component: AdministrationComponent,
            },
            {
                path: 'finance',
                component: FinanceComponent
            },
            {
                path: 'information-technology',
                component: InformationTechnologyComponent
            },
            {
                path: 'sales',
                component: SalesComponent
            }
        ]
    },
];
export var WorkersModule = (function () {
    function WorkersModule() {
    }
    WorkersModule = __decorate([
        NgModule({
            imports: [
                CommonModule,
                RouterModule.forRoot(appRoutesWorker),
                Ng2SmartTableModule
            ],
            declarations: [
                WorkersComponent,
                InformationTechnologyComponent,
                SalesComponent,
                AdministrationComponent,
                FinanceComponent],
            exports: [
                WorkersComponent,
                InformationTechnologyComponent,
                SalesComponent,
                AdministrationComponent,
                FinanceComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], WorkersModule);
    return WorkersModule;
}());
// const appRoutes: Routes = [
//   {
//     path: 'workers', component: WorkersComponent
//   },
//   {path: 'administration', component: AdministrationComponent},
//   {path: 'finance', component: FinanceComponent},
//   {path: 'information-technology', component: InformationTechnologyComponent},
//   {path: 'sales', component: SalesComponent},
//
//   // children: [
//   //     // {path: '', redirectTo: 'workers', pathMatch: 'full'},
//   //     {path: 'administration', component: AdministrationComponent},
//   //     {path: 'finance', component: FinanceComponent},
//   //     {path: 'information-technology', component: InformationTechnologyComponent},
//   //     {path: 'sales', component: SalesComponent},
//   //
//   //
//   //   ]
//
// ];
//# sourceMappingURL=D:/workers/app/src/src/app/workers/workers.module.js.map