import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DashboardService {

  constructor(private http: Http) {
      console.log('DashboardService Initialized...');
  }

  getDashboardData(){
      return this.http.get('http://localhost:3000/dashboard').map(res => res.json());
  }


}
