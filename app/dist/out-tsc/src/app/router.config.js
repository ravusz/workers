var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { UIRouter } from "ui-router-ng2";
import { Injectable } from "@angular/core";
/**
 * Create your own configuration class (if necessary) for any root/feature/lazy module.
 *
 * Pass it to the UIRouterModule.forRoot/forChild factory methods as `configClass`.
 *
 * The class will be added to the Injector and instantiate when the module loads.
 */
export var MyRootUIRouterConfig = (function () {
    /** You may inject dependencies into the constructor */
    function MyRootUIRouterConfig(uiRouter) {
        // Show the ui-router visualizer
        var vis = window['ui-router-visualizer'];
        vis.visualizer(uiRouter);
    }
    MyRootUIRouterConfig = __decorate([
        Injectable(), 
        __metadata('design:paramtypes', [UIRouter])
    ], MyRootUIRouterConfig);
    return MyRootUIRouterConfig;
}());
//# sourceMappingURL=D:/workers/app/src/src/app/router.config.js.map