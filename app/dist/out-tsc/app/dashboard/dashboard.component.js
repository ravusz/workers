var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DashboardService } from './dashboard.service';
export var DashboardComponent = (function () {
    function DashboardComponent(dashboardService) {
        var _this = this;
        this.dashboardService = dashboardService;
        this.name = 'rafal';
        this.surname = 'ubysz';
        this.id = 2;
        this.dashboardService.getDashboardData().subscribe(function (dashboardData) {
            _this.name = dashboardData.name;
            _this.surname = dashboardData.surname;
            _this.id = dashboardData.id;
            console.log(_this.dashboardData);
        });
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    DashboardComponent = __decorate([
        Component({
            selector: 'app-dashboard',
            templateUrl: './dashboard.component.html',
            styleUrls: ['./dashboard.component.css'],
            providers: [DashboardService]
        }), 
        __metadata('design:paramtypes', [DashboardService])
    ], DashboardComponent);
    return DashboardComponent;
}());
//# sourceMappingURL=D:/Project/app/src/app/dashboard/dashboard.component.js.map