//import Angular library
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule, Routes} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {Ng2SmartTableModule} from 'ng2-smart-table';


import {CoreModule} from './core/core.module';
import {WorkersModule} from './workers/workers.module';
// import {WorkersComponent} from './workers/workers.component';


// import Components
import {AppComponent} from './app.component';
import {PublicComponent} from './public/public.component';
import {DashboardComponent} from './dashboard/dashboard.component';


const appRoutes: Routes = [
  {path: '', redirectTo: 'public', pathMatch: 'full'},
  {
    path: 'public',
    component: PublicComponent,
    children: [{
      path: 'dashboard',
      component: DashboardComponent,

    }]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    PublicComponent,
    // WorkersComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    CoreModule,
    WorkersModule,
    HttpModule,
    Ng2SmartTableModule,
    RouterModule.forRoot(appRoutes),
    NgbModule.forRoot()
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}


// npm install --save @ngtools/webpack@1.2.4
