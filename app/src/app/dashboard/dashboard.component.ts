import {Component, OnInit} from '@angular/core';

import {DashboardService} from './dashboard.service';

interface DashboardData {
  id: number;
  name: string;
  surname: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [DashboardService]
})
export class DashboardComponent implements OnInit {
  name: string;
  surname: string;
  id: number;
  dashboardData: DashboardData[];

  constructor(private dashboardService: DashboardService) {
    this.name = 'rafal';
    this.surname = 'ubysz';
    this.id = 2;

    this.dashboardService.getDashboardData().subscribe(dashboardData => {
      this.name = dashboardData.name;
      this.surname = dashboardData.surname;
      this.id = dashboardData.id;

      console.log(this.dashboardData);
    });
  }

  ngOnInit() {


  }

}

