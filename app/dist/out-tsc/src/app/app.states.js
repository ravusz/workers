import { AppComponent } from "./app.component";
import { loadNgModule } from "ui-router-ng2";
/** The top level state(s) */
export var MAIN_STATES = [
    // The top-level app state.
    // This state fills the root <ui-view></ui-view> (defined in index.html) with the AppComponent
    { name: 'app', component: AppComponent },
    // This is the Future State for lazy loading the BazModule
    { name: 'app.baz', url: '/baz', lazyLoad: loadNgModule('src/baz/baz.module') }
];
//# sourceMappingURL=D:/workers/app/src/src/app/app.states.js.map