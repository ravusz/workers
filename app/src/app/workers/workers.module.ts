import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {CommonModule} from '@angular/common';
import {Ng2SmartTableModule} from 'ng2-smart-table';


import {InformationTechnologyComponent} from './departments/information-technology/information-technology.component';
import {SalesComponent} from './departments/sales/sales.component';
import {AdministrationComponent} from './departments/administration/administration.component';
import {FinanceComponent} from './departments/finance/finance.component';

import {WorkersComponent} from './workers.component';


const appRoutesWorker: Routes = [
  {
    path: 'workers',
    component: WorkersComponent,
    children: [{
      path: 'administration',
      component: AdministrationComponent,

    },
      {
        path: 'finance',
        component: FinanceComponent
      },
      {
        path: 'information-technology',
        component: InformationTechnologyComponent

      },
      {
        path: 'sales',
        component: SalesComponent
      }
    ]
  },
  // children: [
  //     // {path: '', redirectTo: 'workers', pathMatch: 'full'},
  //     {path: 'administration', component: AdministrationComponent},
  //     {path: 'finance', component: FinanceComponent},
  //     {path: 'information-technology', component: InformationTechnologyComponent},
  //     {path: 'sales', component: SalesComponent},
  //
  //
  //   ]


];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(appRoutesWorker),
    Ng2SmartTableModule
  ],
  declarations: [
    WorkersComponent,
    InformationTechnologyComponent,
    SalesComponent,
    AdministrationComponent,
    FinanceComponent],
  exports: [
    WorkersComponent,
    InformationTechnologyComponent,
    SalesComponent,
    AdministrationComponent,
    FinanceComponent]
})
export class WorkersModule {
}


// const appRoutes: Routes = [
//   {
//     path: 'workers', component: WorkersComponent
//   },
//   {path: 'administration', component: AdministrationComponent},
//   {path: 'finance', component: FinanceComponent},
//   {path: 'information-technology', component: InformationTechnologyComponent},
//   {path: 'sales', component: SalesComponent},
//
//   // children: [
//   //     // {path: '', redirectTo: 'workers', pathMatch: 'full'},
//   //     {path: 'administration', component: AdministrationComponent},
//   //     {path: 'finance', component: FinanceComponent},
//   //     {path: 'information-technology', component: InformationTechnologyComponent},
//   //     {path: 'sales', component: SalesComponent},
//   //
//   //
//   //   ]
//
// ];
