import { async, TestBed } from '@angular/core/testing';
import { InformationTechnologyComponent } from './information-technology.component';
describe('InformationTechnologyComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [InformationTechnologyComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(InformationTechnologyComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=D:/workers/app/src/src/app/workers/departments/information-technology/information-technology.component.spec.js.map