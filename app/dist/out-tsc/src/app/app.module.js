var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { CoreModule } from './core/core.module';
import { WorkersModule } from './workers/workers.module';
import { AppComponent } from './app.component';
import { PublicComponent } from './public/public.component';
import { DashboardComponent } from './dashboard/dashboard.component';
var appRoutes = [
    { path: '', redirectTo: 'public', pathMatch: 'full' },
    {
        path: 'public',
        component: PublicComponent,
        children: [{
                path: 'dashboard',
                component: DashboardComponent,
            }]
    }
];
console.log('CoreModule', CoreModule);
export var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        NgModule({
            declarations: [
                AppComponent,
                DashboardComponent,
                PublicComponent,
            ],
            imports: [
                BrowserModule,
                FormsModule,
                CoreModule,
                WorkersModule,
                HttpModule,
                Ng2SmartTableModule,
                RouterModule.forRoot(appRoutes),
                NgbModule.forRoot()
            ],
            bootstrap: [AppComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
// npm install --save @ngtools/webpack@1.2.4
//# sourceMappingURL=D:/workers/app/src/src/app/app.module.js.map